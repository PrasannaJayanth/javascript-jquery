document.getElementById("fname").addEventListener('change', validatefname); //call function
document.getElementById("lname").addEventListener("change", validatelname);
document.getElementById("number").addEventListener("change", validatenumber);
document.getElementById("email").addEventListener("change", validateEmail);
document.getElementById("street").addEventListener("change", validateStreet);
document.getElementById("city").addEventListener("change", validateCity);
document.getElementById("state").addEventListener("change", validateState);
document.getElementById("country").addEventListener("change", validateCountry);
document.getElementById("pin").addEventListener("change", validatePin);
document.getElementById("file").addEventListener("change", validateFile);
document.getElementById("form").addEventListener("submit", validateForm);

const neverUse = /^\S*$/; // pattern for checking
function validatefname() {
    const fname = document.getElementById("fname");
    if (neverUse.test(fname.value)) {
        fname.classList.remove('is-invalid');
        fname.classList.add('is-valid');
        return true;
    } else {
        fname.classList.remove('is-valid');

        fname.classList.add('is-invalid');
        return false;
    }
}
function validatelname() {
    const lname = document.getElementById('lname');
    if (neverUse.test(lname.value)) {
        lname.classList.remove("is-invalid");
        lname.classList.add('is-valid');
        return true;
    } else {
        lname.classList.remove("is-valid");
        lname.classList.add('is-invalid');
        return false;
    }
}
function validatenumber() {
    let number = document.getElementById('number');
    var phoneno = /^\d{10}$/;
    const ExactNumber = number.value;
    if (phoneno.test(ExactNumber)) {
        number.classList.remove("is-invalid");
        number.classList.add("is-valid");
        return true;
    } else {
        number.classList.remove("is-valid");
        number.classList.add("is-invalid");
        return false;
    }
}
function validateEmail() {
    let email = document.getElementById("email");
    const noUseEmail = /^([a-zA-Z0-9_\-?\.?]){3,}@([a-zA-Z]){3,}\.([a-zA-Z]){2,5}$/;
    if (noUseEmail.test(email.value)) {
        email.classList.remove("is-invalid");
        email.classList.add("is-valid");
    } else {
        email.classList.remove("is-valid");
        email.classList.add("is-invalid");
    }
}

function validateStreet() {
    let street = document.getElementById("street");
    const streetValue = /^[a-zA-Z0-9\s,'/\-]*$/;
    if (streetValue.test(street.value)) {
        street.classList.remove("is-invalid");
        street.classList.add("is-valid");
    } else {
        street.classList.remove("is-valid");
        street.classList.add("is-invalid");
    }
}
function validateCity() {
    let city = document.getElementById("city");
    const CityValue = /^[A-Za-z\s-,()]+$/;

    if (CityValue.test(city.value)) {
        city.classList.remove("is-invalid");
        city.classList.add("is-valid");
    } else {
        city.classList.remove("is-valid");
        city.classList.add("is-invalid");
    }
}
function validateState() {
    let state = document.getElementById("state");
    const StateValue = /^[A-Za-z\s-]+$/;

    if (StateValue.test(state.value)) {
        state.classList.remove("is-invalid");
        state.classList.add("is-valid");
    } else {
        state.classList.remove("is-valid");
        state.classList.add("is-invalid");
    }
}
function validateCountry() {
    let country = document.getElementById("country");
    const CountryValue = /^[A-Za-z\s-]+$/;

    if (CountryValue.test(country.value)) {
        country.classList.remove("is-invalid");
        country.classList.add("is-valid");
    } else {
        country.classList.remove("is-valid");
        country.classList.add("is-invalid");
    }
}
function validatePin() {
    let pin = document.getElementById("pin");
    const PinValue = /^\d{6}$/;
    if (PinValue.test(pin.value)) {
        pin.classList.remove("is-invalid");
        pin.classList.add("is-valid");
    } else {
        pin.classList.remove("is-valid");
        pin.classList.add("is-invalid");
    }

}
function validateFile() {
    let file = document.getElementById("file");
    const pdfFileValue = /\.pdf$/i;
    if (pdfFileValue.test(file.value)) {
        file.classList.remove("is-invalid");
        file.classList.add("is-valid");
    } else {
        file.classList.remove("is-valid");
        file.classList.add("is-invalid");
    }
}
function validateForm(e) {
    e.preventDefault();
 
    let fname = document.getElementById("fname");
    let lname = document.getElementById("lname");
    let number = document.getElementById("number");
    let email = document.getElementById("email");
    let street = document.getElementById("street");
    let city = document.getElementById("city");
    let state = document.getElementById("state");
    let country = document.getElementById("country");
    let pin = document.getElementById("pin");
    let file = document.getElementById("file");

    const neverUse = /^\S*$/; 
    if (fname.value === "" )   {
       
        fname.classList.add("is-invalid");
        lname.classList.remove("is-valid");
    }
    else {
        fname.classList.remove("is-invalid");
        fname.classList.add("is-valid");
    }
    if (lname.value === "" ) {
        lname.classList.add("is-invalid");
        lname.classList.remove("is-valid");

    } else {
        lname.classList.remove("is-invalid");
        lname.classList.add("is-valid");
    }
    if (number.value === "") {
        number.classList.add("is-invalid");
        number.classList.remove("is-valid")
    } else {
        number.classList.remove("is-invalid");
        number.classList.add("is-valid")
    }
    if (email.value === "") {
        email.classList.add("is-invalid");
        email.classList.remove("is-valid")
    } else {
        email.classList.remove("is-invalid");
        email.classList.add("is-valid")
    }
    if (street.value === "") {
        street.classList.add("is-invalid");
        street.classList.remove("is-valid")
    } else {
        street.classList.remove("is-invalid");
        street.classList.add("is-valid")
    }
    if (city.value === "") {
        city.classList.add("is-invalid");
        city.classList.remove("is-valid")
    } else {
        city.classList.remove("is-invalid");
        city.classList.add("is-valid")
    }
    if (state.value === "") {
        state.classList.add("is-invalid");
        state.classList.remove("is-valid")
    } else {
        state.classList.remove("is-invalid");
        state.classList.add("is-valid")
    }
    if (country.value === "") {
        country.classList.add("is-invalid");
        country.classList.remove("is-valid")
    } else {
        country.classList.remove("is-invalid");
        country.classList.add("is-valid")
    }
    if (pin.value === "") {
        pin.classList.add("is-invalid");
        pin.classList.remove("is-valid")
    } else {
        pin.classList.remove("is-invalid");
        pin.classList.add("is-valid")
    }
    if (file.value === "") {
        file.classList.add("is-invalid");
        file.classList.remove("is-valid")
    } else {
        file.classList.remove("is-invalid");
        file.classList.add("is-valid")
    }

    

    
    




    

    
    



}





